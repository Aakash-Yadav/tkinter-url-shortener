from tkinter import *
from requests import post 
import time 
import pyperclip as pr 
from concurrent.futures import ThreadPoolExecutor



root = Tk()
root.geometry("260x150")
root.title('URL-Shortener')
root.resizable(False,False)


def ThreadPoolExecutor_(n):
    with ThreadPoolExecutor() as exe:
        name =exe.map(get_api,[n])
    return [x for x in name][-1]


def get_api(x):
    api = post('https://toy.pythonanywhere.com//Shortener',data={
        'URL':x
    }).json()
    return api['short']


def op():
    x= ThreadPoolExecutor_(a1.get())
    a1.delete(0,END)
    print(x)
    a1.insert(0,x[-1])
    pr.copy(x)
    a1.delete(0,END)   
def cl():
    a1.delete(0,END)


a1 = Entry(root,width=10,font=("Helvetica",25))
a1.place(x = 30,y=20)

bt = Button(root,text="Click",bg="pink",command=op)
bt.place(x=170,y=100)

b1 = Button(root,text="Clear",bg="pink",command=cl)
b1.place(x=30,y=100)

mainloop()

'toy.pythonanywhere.com/1rDFg'
